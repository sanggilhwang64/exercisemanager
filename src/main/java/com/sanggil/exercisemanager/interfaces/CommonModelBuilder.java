package com.sanggil.exercisemanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
