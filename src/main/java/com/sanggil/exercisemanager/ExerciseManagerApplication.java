package com.sanggil.exercisemanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ExerciseManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ExerciseManagerApplication.class, args);
    }

}
