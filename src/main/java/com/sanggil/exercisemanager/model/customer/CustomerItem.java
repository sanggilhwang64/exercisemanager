package com.sanggil.exercisemanager.model.customer;

import com.sanggil.exercisemanager.entity.Customer;
import com.sanggil.exercisemanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerItem {
    @ApiModelProperty(notes = "시퀀스", required = true)
    private Long id;
    @ApiModelProperty(notes = "고객명", required = true)
    private String customerName;
    @ApiModelProperty(notes = "키", required = true)
    private Double height;
    @ApiModelProperty(notes = "몸무게", required = true)
    private Double weight;
    @ApiModelProperty(notes = "bmi", required = true)
    private Double bmi;
    @ApiModelProperty(notes = "비만도", required = true)
    private String bmiResultName;


    private CustomerItem(CustomerItemBuilder builder) {
        this.id = builder.id;
        this.customerName = builder.customerName;
        this.height = builder.height;
        this.weight = builder.weight;
        this.bmi = builder.bmi;
        this.bmiResultName = builder.bmiResultName;
    }

    public static class CustomerItemBuilder implements CommonModelBuilder<CustomerItem> {
        private final Long id;
        private final String customerName;
        private final Double height;
        private final Double weight;
        private final Double bmi;
        private final String bmiResultName;

        public CustomerItemBuilder(Customer customer) {
            this.id = customer.getId();
            this.customerName = customer.getCustomerName();
            this.height = customer.getHeight();
            this.weight = customer.getWeight();
            this.bmi = customer.getWeight() / ((customer.getHeight() / 100) * (customer.getHeight() / 100));

            String bmiResultName = "";
            if (this.bmi <= 18.5) {
                bmiResultName = "저체중";
            } else if (this.bmi <= 22.9) {
                bmiResultName = "정상";
            } else if (this.bmi <= 24.9) {
                bmiResultName = "과체중";
            } else {
                bmiResultName = "비만";
            }
            this.bmiResultName = bmiResultName;
        }
        @Override
        public CustomerItem build() {
            return new CustomerItem(this);
        }
    }
}
