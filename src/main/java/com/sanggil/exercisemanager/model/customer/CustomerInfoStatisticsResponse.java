package com.sanggil.exercisemanager.model.customer;

import com.sanggil.exercisemanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CustomerInfoStatisticsResponse {
    @ApiModelProperty(notes = "총 고객 수", required = true)
    private Long totalCustomerCount;

    @ApiModelProperty(notes = "평균 키", required = true)
    private Double averageHeight;

    @ApiModelProperty(notes = "평균 몸무게", required = true)
    private Double averageWeight;

    @ApiModelProperty(notes = "초보 고객 수", required = true)
    private Long lowCustomerCount;

    @ApiModelProperty(notes = "중급 고객 수", required = true)
    private Long middleCustomerCount;

    @ApiModelProperty(notes = "고급 고객 수", required = true)
    private Long highCustomerCount;


    private CustomerInfoStatisticsResponse(CustomerInfoStatisticsResponseBuilder builder) {
        this.totalCustomerCount = builder.totalCustomerCount;
        this.averageHeight = builder.averageHeight;
        this.averageWeight = builder.averageWeight;
        this.lowCustomerCount = builder.lowCustomerCount;
        this.middleCustomerCount = builder.middleCustomerCount;
        this.highCustomerCount = builder.highCustomerCount;
    }

    public static class CustomerInfoStatisticsResponseBuilder implements CommonModelBuilder<CustomerInfoStatisticsResponse> {
        private final Long totalCustomerCount;
        private final Double averageHeight;
        private final Double averageWeight;
        private final Long lowCustomerCount;
        private final Long middleCustomerCount;
        private final Long highCustomerCount;

        public CustomerInfoStatisticsResponseBuilder(
                Long totalUserCount,
                Double averageHeight,
                Double averageWeight,
                Long lowCustomerCount,
                Long middleCustomerCount,
                Long highCustomerCount
        ){
            this.totalCustomerCount = totalUserCount;
            this.averageHeight = averageHeight;
            this.averageWeight = averageWeight;
            this.lowCustomerCount = lowCustomerCount;
            this.middleCustomerCount = middleCustomerCount;
            this.highCustomerCount = highCustomerCount;
        }

        @Override
        public CustomerInfoStatisticsResponse build() {
            return new CustomerInfoStatisticsResponse(this);
        }
    }
}
