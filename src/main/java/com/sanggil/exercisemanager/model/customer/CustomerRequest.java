package com.sanggil.exercisemanager.model.customer;

import com.sanggil.exercisemanager.enums.Ability;
import com.sanggil.exercisemanager.enums.Gender;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {
    @ApiModelProperty(notes = "성별", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @ApiModelProperty(notes = "고객명(2~20자)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @ApiModelProperty(notes = "키", required = true)
    @NotNull
    private Double height;

    @ApiModelProperty(notes = "몸무게", required = true)
    @NotNull
    private Double weight;

    @ApiModelProperty(notes = "능력", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Ability ability;
}
