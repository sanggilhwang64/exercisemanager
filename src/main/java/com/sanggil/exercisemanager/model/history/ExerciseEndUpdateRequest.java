package com.sanggil.exercisemanager.model.history;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalTime;

@Getter
@Setter
public class ExerciseEndUpdateRequest {
    @ApiModelProperty(notes = "운동 종료 여부", required = true)
    @NotNull
    private Boolean isFinished;
    @ApiModelProperty(notes = "운동 종료 시간", required = true)
    private LocalTime finishExercise;
}
