package com.sanggil.exercisemanager.model.history;

import com.sanggil.exercisemanager.entity.ExerciseHistory;
import com.sanggil.exercisemanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryItem {
    @ApiModelProperty(notes = "운동기록 id", required = true)
    private Long historyId;
    @ApiModelProperty(notes = "고객 id", required = true)
    private Long customerId;
    @ApiModelProperty(notes = "고객명", required = true)
    private String customerName;
    @ApiModelProperty(notes = "성별", required = true)
    private String gender;
    @ApiModelProperty(notes = "키", required = true)
    private Double height;
    @ApiModelProperty(notes = "몸무게", required = true)
    private Double weight;
    @ApiModelProperty(notes = "능력", required = true)
    private String ability;
    @ApiModelProperty(notes = "운동 이름", required = true)
    private String exerciseName;
    @ApiModelProperty(notes = "운동 부위", required = true)
    private String part;

    private HistoryItem(HistoryItemBuilder builder) {
        this.historyId = builder.historyId;
        this.customerId = builder.customerId;
        this.customerName = builder.customerName;
        this.gender = builder.gender;
        this.height = builder.height;
        this.weight = builder.weight;
        this.ability = builder.ability;
        this.exerciseName = builder.exerciseName;
        this.part = builder.part;
    }

    public static class HistoryItemBuilder implements CommonModelBuilder<HistoryItem> {
        private final Long historyId;
        private final Long customerId;
        private final String customerName;
        private final String gender;
        private final Double height;
        private final Double weight;
        private final String ability;
        private final String exerciseName;
        private final String part;

        public HistoryItemBuilder(ExerciseHistory exerciseHistory) {
            this.historyId = exerciseHistory.getId();
            this.customerId = exerciseHistory.getCustomer().getId();
            this.customerName = exerciseHistory.getCustomer().getCustomerName();
            this.gender = exerciseHistory.getCustomer().getGender().getGenderName();
            this.height = exerciseHistory.getCustomer().getHeight();
            this.weight = exerciseHistory.getCustomer().getWeight();
            this.ability = exerciseHistory.getCustomer().getAbility().getAbilityName();
            this.exerciseName = exerciseHistory.getExerciseName();
            this.part = exerciseHistory.getPart().getPartName();
        }

        @Override
        public HistoryItem build() {
            return new HistoryItem(this);
        }
    }
}
