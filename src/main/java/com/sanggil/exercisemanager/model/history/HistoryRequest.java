package com.sanggil.exercisemanager.model.history;

import com.sanggil.exercisemanager.enums.Part;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HistoryRequest {
    @ApiModelProperty(notes = "운동 이름 (2~20)", required = true)
    @NotNull
    @Length(min = 2, max = 20)
    private String exerciseName;
    @ApiModelProperty(notes = "운동 중량", required = true)
    @NotNull
    private Integer exerciseWeight;
    @ApiModelProperty(notes = "운동 몇 세트", required = true)
    @NotNull
    private Integer exerciseSet;
    @ApiModelProperty(notes = "운동 몇 개", required = true)
    @NotNull
    private Integer exerciseNumber;
    @ApiModelProperty(notes = "운동 부위", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private Part part;
}
