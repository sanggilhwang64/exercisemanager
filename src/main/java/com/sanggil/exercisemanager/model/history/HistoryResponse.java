package com.sanggil.exercisemanager.model.history;

import com.sanggil.exercisemanager.entity.ExerciseHistory;
import com.sanggil.exercisemanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class HistoryResponse {
    @ApiModelProperty(notes = "고객 id", required = true)
    private Long customerId;
    @ApiModelProperty(notes = "성별", required = true)
    private String gender;
    @ApiModelProperty(notes = "고객명", required = true)
    private String customerName;
    @ApiModelProperty(notes = "키", required = true)
    private Double height;
    @ApiModelProperty(notes = "몸무게", required = true)
    private Double weight;
    @ApiModelProperty(notes = "bmi", required = true)
    private Double bmi;
    @ApiModelProperty(notes = "비만도", required = true)
    private String bmiResultName;
    @ApiModelProperty(notes = "능력", required = true)
    private String ability;
    @ApiModelProperty(notes = "운동 이름", required = true)
    private String exerciseName;
    @ApiModelProperty(notes = "운동 중량", required = true)
    private Integer exerciseWeight;
    @ApiModelProperty(notes = "운동 몇 세트", required = true)
    private Integer exerciseSet;
    @ApiModelProperty(notes = "운동 몇 개", required = true)
    private Integer exerciseNumber;
    @ApiModelProperty(notes = "운동 부위", required = true)
    private String part;
    @ApiModelProperty(notes = "운동 시작 날짜", required = true)
    private LocalDate startDate;
    @ApiModelProperty(notes = "마지막 운동 날짜", required = true)
    private LocalDate lastDate;

    private HistoryResponse(HistoryResponseBuilder builder) {
        this.customerId = builder.customerId;
        this.gender = builder.gender;
        this.customerName = builder.customerName;
        this.height = builder.height;
        this.weight = builder.weight;
        this.bmi = builder.bmi;
        this.bmiResultName = builder.bmiResultName;
        this.ability = builder.ability;
        this.exerciseName = builder.exerciseName;
        this.exerciseWeight = builder.exerciseWeight;
        this.exerciseSet = builder.exerciseSet;
        this.exerciseNumber = builder.exerciseNumber;
        this.part = builder.part;
        this.startDate = builder.startDate;
        this.lastDate = builder.lastDate;
    }

    public static class HistoryResponseBuilder implements CommonModelBuilder<HistoryResponse> {
        private final Long customerId;
        private final String gender;
        private final String customerName;
        private final Double height;
        private final Double weight;
        private final Double bmi;
        private final String bmiResultName;
        private final String ability;
        private final String exerciseName;
        private final Integer exerciseWeight;
        private final Integer exerciseSet;
        private final Integer exerciseNumber;
        private final String part;
        private final LocalDate startDate;
        private final LocalDate lastDate;

        public HistoryResponseBuilder(ExerciseHistory exerciseHistory) {
            this.customerId = exerciseHistory.getId();
            this.gender = exerciseHistory.getCustomer().getGender().getGenderName();
            this.customerName = exerciseHistory.getCustomer().getCustomerName();
            this.height = exerciseHistory.getCustomer().getHeight();
            this.weight = exerciseHistory.getCustomer().getWeight();
            this.bmi = exerciseHistory.getCustomer().getWeight() /
                    ((exerciseHistory.getCustomer().getHeight() / 100) * (exerciseHistory.getCustomer().getHeight() / 100));

            String bmiResultName = "";
            if (this.bmi <= 18.5) {
                bmiResultName = "저체중";
            } else if (this.bmi <= 22.9) {
                bmiResultName = "정상";
            } else if (this.bmi <= 24.9) {
                bmiResultName = "과체중";
            } else {
                bmiResultName = "비만";
            }
            this.bmiResultName = bmiResultName;
            this.ability = exerciseHistory.getCustomer().getAbility().getAbilityName();
            this.exerciseName = exerciseHistory.getExerciseName();
            this.exerciseWeight = exerciseHistory.getExerciseWeight();
            this.exerciseSet = exerciseHistory.getExerciseSet();
            this.exerciseNumber = exerciseHistory.getExerciseNumber();
            this.part = exerciseHistory.getPart().getPartName();
            this.startDate = exerciseHistory.getStartDate();
            this.lastDate = exerciseHistory.getCustomer().getLastDate();
        }


        @Override
        public HistoryResponse build() {
            return new HistoryResponse(this);
        }
    }
}
