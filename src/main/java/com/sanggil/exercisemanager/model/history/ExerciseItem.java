package com.sanggil.exercisemanager.model.history;

import com.sanggil.exercisemanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseItem {
    @ApiModelProperty(notes = "부위 이름", required = true)
    private String partName;
    @ApiModelProperty(notes = "운동 시간", required = true)
    private Long runningTime;
    @ApiModelProperty(notes = "평균 몸무게", required = true)
    private Integer averageWeight;
    @ApiModelProperty(notes = "총 개수", required = true)
    private Integer totalCount;

    private ExerciseItem(ExerciseItemBuilder builder) {
        this.partName = builder.partName;
        this.runningTime = builder.runningTime;
        this.averageWeight = builder.averageWeight;
        this.totalCount = builder.totalCount;
    }

    public static class ExerciseItemBuilder implements CommonModelBuilder<ExerciseItem> {
        private final String partName;
        private final Long runningTime;
        private final Integer averageWeight;
        private final Integer totalCount;

        public ExerciseItemBuilder(String partName, Long runningTime, Integer averageWeight, Integer totalCount) {
            this.partName = partName;
            this.runningTime = runningTime;
            this.averageWeight = averageWeight;
            this.totalCount = totalCount;
        }


        @Override
        public ExerciseItem build() {
            return new ExerciseItem(this);
        }
    }
}
