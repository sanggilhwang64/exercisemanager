package com.sanggil.exercisemanager.model.history;

import com.sanggil.exercisemanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseResponse {
    @ApiModelProperty(notes = "총 몇분 했는지", required = true)
    private String totalRunningTimeName;
    @ApiModelProperty(notes = "오늘 운동 부위", required = true)
    private String todayParts;
    @ApiModelProperty(notes = "운동 아이템", required = true)
    private List<ExerciseItem> exerciseItem;

    private ExerciseResponse(ExerciseResponseBuilder builder) {
        this.totalRunningTimeName = builder.totalRunningTimeName;
        this.todayParts = builder.todayParts;
        this.exerciseItem = builder.exerciseItem;
    }

    public static class ExerciseResponseBuilder implements CommonModelBuilder<ExerciseResponse> {
        private final String totalRunningTimeName;
        private final String todayParts;
        private final List<ExerciseItem> exerciseItem;

        public ExerciseResponseBuilder(String totalRunningTimeName, String todayParts, List<ExerciseItem> exerciseItem) {
            this.totalRunningTimeName = totalRunningTimeName;
            this.todayParts = todayParts;
            this.exerciseItem = exerciseItem;
        }

        @Override
        public ExerciseResponse build() {
            return new ExerciseResponse(this);
        }
    }
}
