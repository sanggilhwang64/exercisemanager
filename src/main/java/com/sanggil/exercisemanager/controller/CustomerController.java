package com.sanggil.exercisemanager.controller;

import com.sanggil.exercisemanager.model.*;
import com.sanggil.exercisemanager.model.customer.CustomerInfoStatisticsResponse;
import com.sanggil.exercisemanager.model.customer.CustomerInfoUpdateRequest;
import com.sanggil.exercisemanager.model.customer.CustomerItem;
import com.sanggil.exercisemanager.model.customer.CustomerRequest;
import com.sanggil.exercisemanager.service.CustomerService;
import com.sanggil.exercisemanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "고객 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @ApiOperation(value = "고객 등록")
    @PostMapping("/new")
    public CommonResult setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "고객 목록")
    @GetMapping("/all")
    public ListResult<CustomerItem> getCustomers() {
        return ResponseService.getListResult(customerService.getCustomers(),true);
    }

    @ApiOperation(value = "고객 통계")
    @GetMapping("/statisticsCustomer")
    public SingleResult<CustomerInfoStatisticsResponse> getStatisticsByUser() {
        return ResponseService.getSingleResult(customerService.getStatisticsByUser());
    }

    @ApiOperation(value = "고객 정보 수정")
    @PutMapping("/info/{id}")
    public CommonResult putCustomerInfo(@PathVariable long id, @RequestBody @Valid CustomerInfoUpdateRequest updateRequest) {
        customerService.putCustomerInfo(id, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "고객 정보 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delCustomer(@PathVariable long id) {
        customerService.delCustomer(id);

        return ResponseService.getSuccessResult();
    }

}
