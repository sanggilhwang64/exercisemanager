package com.sanggil.exercisemanager.controller;

import com.sanggil.exercisemanager.entity.Customer;
import com.sanggil.exercisemanager.model.CommonResult;
import com.sanggil.exercisemanager.model.SingleResult;
import com.sanggil.exercisemanager.model.history.*;
import com.sanggil.exercisemanager.service.CustomerService;
import com.sanggil.exercisemanager.service.HistoryService;
import com.sanggil.exercisemanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "운동 기록 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {
    private final HistoryService historyService;
    private final CustomerService customerService;

    @ApiOperation(value = "운동 기록 등록")
    @PostMapping("/new/customer-id/{customerId}")
    public CommonResult setHistory(@PathVariable long customerId, @RequestBody @Valid HistoryRequest request) {
        Customer customer = customerService.getData(customerId);

        historyService.setHistory(customer, request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "운동 기록 상세정보")
    @GetMapping("/detail/history/{historyId}")
    public SingleResult<HistoryResponse> getDetailHistory(@PathVariable long historyId) {
        return ResponseService.getSingleResult(historyService.getDetailHistory(historyId));
    }

    @ApiOperation(value = "운동 기록 통계")
    @GetMapping("/exercise/{exerciseCustomerId}")
    public SingleResult<ExerciseResponse> getStatisticsByExercise(@PathVariable long exerciseCustomerId) {
        return ResponseService.getSingleResult(historyService.getStatisticsByExercise(exerciseCustomerId));
    }

    @ApiOperation(value = "운동 기록 수정")
    @PutMapping("/exerciseInfo/{id}")
    public CommonResult putExerciseInfo(@PathVariable long id, @RequestBody @Valid HistoryExerciseUpdateRequest updateRequest) {
        historyService.putExerciseInfo(id, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "운동 종료 정보 수정")
    @PutMapping("/exerciseEnd/{historyId}")
    public CommonResult putExerciseEnd(@PathVariable long historyId) {
        historyService.putExerciseEnd(historyId);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "운동 기록 삭제")
    @DeleteMapping("/{id}")
    public CommonResult delHistory(@PathVariable long id) {
        historyService.delHistory(id);

        return ResponseService.getSuccessResult();
    }
}
