package com.sanggil.exercisemanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Ability {
    LOW("초보"),
    MIDDLE("중급"),
    HIGH("상급")
    ;
    private final String abilityName;
}
