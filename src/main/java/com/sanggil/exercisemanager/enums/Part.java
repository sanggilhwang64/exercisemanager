package com.sanggil.exercisemanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Part {
    BACK("등"),
    CHEST("가슴"),
    SHOULDER("어깨"),
    ARM("팔"),
    LEG("다리")
    ;
    private final String partName;
}
