package com.sanggil.exercisemanager.service;

import com.sanggil.exercisemanager.entity.Customer;
import com.sanggil.exercisemanager.enums.Ability;
import com.sanggil.exercisemanager.exception.CMissingDataException;
import com.sanggil.exercisemanager.model.customer.CustomerInfoStatisticsResponse;
import com.sanggil.exercisemanager.model.customer.CustomerInfoUpdateRequest;
import com.sanggil.exercisemanager.model.customer.CustomerItem;
import com.sanggil.exercisemanager.model.customer.CustomerRequest;
import com.sanggil.exercisemanager.model.ListResult;
import com.sanggil.exercisemanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer.CustomerBuilder(request).build();

        customerRepository.save(addData);
    }

    public Customer getData(long id) {
        return customerRepository.findById(id).orElseThrow();
    }

    public ListResult<CustomerItem> getCustomers() {
        List<Customer> originList = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        originList.forEach(item -> result.add(new CustomerItem.CustomerItemBuilder(item).build()));

        return ListConvertService.settingResult(result);
    }

    public CustomerInfoStatisticsResponse getStatisticsByUser() {
        List<Customer> originList = customerRepository.findAll();
        // customerRepository에서 전부 가져와라 List<Customer> (원본) 이름은 originList

        long totalUserCount = originList.size();
        double totalHeight = 0; // 총 키의 합 기본값 0
        double totalWeight = 0; // 총 무게의 합 기본값 0
        long lowUserCount = 0; // 초보자 인원수 기본값 0
        long middleUserCount = 0; // 중급자 인원수 기본값 0
        long highUserCount = 0; // 고급자 인원수 기본값 0

        for (Customer item : originList) {  // originList에서 원본을 하나씩 가져온다 이름은 item  for: 반복
            totalHeight += item.getHeight(); //  원본(item)에서 Height를 가져온 후 그걸 총 키의 합에 더한다.
            totalWeight += item.getWeight(); // 원본에서(item)에서 weight를 가져온 후 그걸 총 몸무게의 합에 더한다.

            if (item.getAbility().equals(Ability.LOW)) {
                lowUserCount += 1;
            } else if (item.getAbility().equals(Ability.MIDDLE)) {
                middleUserCount += 1;
            } else if (item.getAbility().equals(Ability.HIGH)) {
                highUserCount += 1;
            }
        }
        return new CustomerInfoStatisticsResponse.CustomerInfoStatisticsResponseBuilder(
                totalUserCount,
                totalHeight / totalUserCount,
                totalWeight / totalUserCount,
                lowUserCount,
                middleUserCount,
                highUserCount
        ).build();
    }

    public void putCustomerInfo(long id, CustomerInfoUpdateRequest updateRequest) {
        Customer originData = customerRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putCustomer(updateRequest);

        customerRepository.save(originData);
    }

    public void delCustomer(long id) {
        customerRepository.deleteById(id);
    }
}
