package com.sanggil.exercisemanager.service;

import com.sanggil.exercisemanager.entity.Customer;
import com.sanggil.exercisemanager.entity.ExerciseHistory;
import com.sanggil.exercisemanager.exception.CMissingDataException;
import com.sanggil.exercisemanager.model.history.*;
import com.sanggil.exercisemanager.repository.HistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final HistoryRepository historyRepository;

    public void setHistory(Customer customer, HistoryRequest historyRequest) {
        ExerciseHistory addData = new ExerciseHistory.ExerciseHistoryBuilder(customer, historyRequest).build();

        historyRepository.save(addData);
    }

    public HistoryResponse getDetailHistory(long id) {
        ExerciseHistory originData = historyRepository.findById(id).orElseThrow();

        return new HistoryResponse.HistoryResponseBuilder(originData).build();
    }

    public ExerciseResponse getStatisticsByExercise(long exerciseCustomerId) {
        List<ExerciseHistory> originList = historyRepository.findAllByCustomer_IdAndStartDate(exerciseCustomerId, LocalDate.now());

        long totalRunningTime = 0;
        List<String> partNames = new LinkedList<>();
        List<ExerciseItem> exerciseItems = new LinkedList<>();

        for (ExerciseHistory item : originList) {
            Duration duration = Duration.between(item.getStartExercise(), item.getFinishExercise());
            long second = duration.getSeconds();
            totalRunningTime += second / 60;

            partNames.add(item.getPart().getPartName());

            ExerciseItem addItem = new ExerciseItem.ExerciseItemBuilder(
                    item.getPart().getPartName(),
                    second / 60,
                    item.getExerciseWeight(),
                    item.getExerciseSet() * item.getExerciseNumber()
            ).build();

            exerciseItems.add(addItem);
        }

        Set<String> partNameResult = new HashSet<>(partNames);
        String partNameResultName = String.join("/", partNameResult);

        return new ExerciseResponse.ExerciseResponseBuilder(totalRunningTime + "분", partNameResultName, exerciseItems).build();
    }


    public void putExerciseInfo(long id, HistoryExerciseUpdateRequest updateRequest) {
        ExerciseHistory originData = historyRepository.findById(id).orElseThrow(CMissingDataException::new);

        originData.putExerciseInfo(updateRequest);

        historyRepository.save(originData);
    }

    public void putExerciseEnd(long id) {
        ExerciseHistory originData = historyRepository.findById(id).orElseThrow(CMissingDataException::new);
        originData.putExerciseEnd();

        historyRepository.save(originData);
    }

    public void delHistory(long id) {
        historyRepository.deleteById(id);
    }
}
