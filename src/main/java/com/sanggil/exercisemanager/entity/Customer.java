package com.sanggil.exercisemanager.entity;

import com.sanggil.exercisemanager.enums.Ability;
import com.sanggil.exercisemanager.enums.Gender;
import com.sanggil.exercisemanager.interfaces.CommonModelBuilder;
import com.sanggil.exercisemanager.model.customer.CustomerInfoUpdateRequest;
import com.sanggil.exercisemanager.model.customer.CustomerRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Customer {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "성별")
    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @ApiModelProperty(notes = "고객명")
    @Column(nullable = false, length = 20)
    private String customerName;

    @ApiModelProperty(notes = "키")
    @Column(nullable = false)
    private Double height;

    @ApiModelProperty(notes = "몸무게")
    @Column(nullable = false)
    private Double weight;

    @ApiModelProperty(notes = "마지막 운동 날짜")
    @Column(nullable = false)
    private LocalDate lastDate;

    @ApiModelProperty(notes = "능력")
    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Ability ability;

    public void putCustomer(CustomerInfoUpdateRequest request) {
        this.customerName = request.getCustomerName();
        this.weight = request.getWeight();
        this.ability = request.getAbility();
    }

    private Customer(CustomerBuilder builder) {
        this.gender = builder.gender;
        this.customerName = builder.customerName;
        this.height = builder.height;
        this.weight = builder.weight;
        this.lastDate = builder.lastDate;
        this.ability = builder.ability;
    }

    public static class CustomerBuilder implements CommonModelBuilder<Customer> {
        private final Gender gender;
        private final String customerName;
        private final Double height;
        private final Double weight;
        private final LocalDate lastDate;
        private final Ability ability;

        public CustomerBuilder(CustomerRequest request) {
            this.gender = request.getGender();
            this.customerName = request.getCustomerName();
            this.height = request.getHeight();
            this.weight = request.getWeight();
            this.lastDate = LocalDate.now();
            this.ability = request.getAbility();
        }

        @Override
        public Customer build() {
            return new Customer(this);
        }
    }
}
