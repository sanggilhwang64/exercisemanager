package com.sanggil.exercisemanager.entity;

import com.sanggil.exercisemanager.enums.Part;
import com.sanggil.exercisemanager.interfaces.CommonModelBuilder;
import com.sanggil.exercisemanager.model.history.ExerciseEndUpdateRequest;
import com.sanggil.exercisemanager.model.history.HistoryExerciseUpdateRequest;
import com.sanggil.exercisemanager.model.history.HistoryRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ExerciseHistory {
    @ApiModelProperty(notes = "시퀀스")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(notes = "고객")
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "exerciseCustomerId", nullable = false)
    private Customer customer;

    @ApiModelProperty(notes = "운동 이름")
    @Column(nullable = false, length = 20)
    private String exerciseName;

    @ApiModelProperty(notes = "운동 중량")
    @Column(nullable = false)
    private Integer exerciseWeight;

    @ApiModelProperty(notes = "운동 몇 세트")
    @Column(nullable = false)
    private Integer exerciseSet;

    @ApiModelProperty(notes = "운동 몇 개")
    @Column(nullable = false)
    private Integer exerciseNumber;

    @ApiModelProperty(notes = "운동 시작 날짜")
    @Column(nullable = false)
    private LocalDate startDate;

    @ApiModelProperty(notes = "운동 부위")
    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Part part;

    @ApiModelProperty(notes = "운동 시작 시간")
    @Column(nullable = false)
    private LocalTime startExercise;

    @ApiModelProperty(notes = "운동 종료 시간")
    @Column(nullable = false)
    private LocalTime finishExercise;
    //종료시간 기본값을 null값으로 하면 get할때 오류가 남 그래서 종료시간과 시작시간을 같게 한다. localtime.now 종료여부 추가
    // 그다음 put으로 종료여부를 true로 바꾸고 finishExercise를 localtime.now로 받아온다.   종료시간 + 시작시간 = 운동한 시간

    @ApiModelProperty(notes = "운동 종료 여부")
    @Column(nullable = false)
    private Boolean isFinished; // 종료여부를 put기능으로 true로 바꾸는 기능을 만듬

    public void putExerciseInfo(HistoryExerciseUpdateRequest request) {
        this.exerciseName = request.getExerciseName();
        this.exerciseWeight = request.getExerciseWeight();
        this.exerciseNumber = request.getExerciseNumber();
        this.part = request.getPart();
    }

    public void putExerciseEnd() {
        this.isFinished = true;
        this.finishExercise = LocalTime.now();
    }

    private ExerciseHistory(ExerciseHistoryBuilder builder) {
        this.customer = builder.customer;
        this.exerciseName = builder.exerciseName;
        this.exerciseWeight = builder.exerciseWeight;
        this.exerciseSet = builder.exerciseSet;
        this.exerciseNumber = builder.exerciseNumber;
        this.startDate = builder.startDate;
        this.part = builder.part;
        this.startExercise = builder.startExercise;
        this.finishExercise = builder.finishExercise;
        this.isFinished = builder.isFinished;
    }

    public static class ExerciseHistoryBuilder implements CommonModelBuilder<ExerciseHistory> {
        private final Customer customer;
        private final String exerciseName;
        private final Integer exerciseWeight;
        private final Integer exerciseSet;
        private final Integer exerciseNumber;
        private final LocalDate startDate;
        private final Part part;
        private final LocalTime startExercise;
        private final LocalTime finishExercise;
        private final Boolean isFinished;

        public ExerciseHistoryBuilder(Customer customer, HistoryRequest historyRequest) {
            this.customer = customer;
            this.exerciseName = historyRequest.getExerciseName();
            this.exerciseWeight = historyRequest.getExerciseWeight();
            this.exerciseSet = historyRequest.getExerciseSet();
            this.exerciseNumber = historyRequest.getExerciseNumber();
            this.startDate = LocalDate.now();
            this.part = historyRequest.getPart();
            this.startExercise = LocalTime.now();
            this.finishExercise = LocalTime.now();
            this.isFinished = false;
        }

        @Override
        public ExerciseHistory build() {
            return new ExerciseHistory(this);
        }
    }
}
