package com.sanggil.exercisemanager.repository;

import com.sanggil.exercisemanager.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
