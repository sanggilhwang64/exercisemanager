package com.sanggil.exercisemanager.repository;

import com.sanggil.exercisemanager.entity.ExerciseHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface HistoryRepository extends JpaRepository<ExerciseHistory, Long> {
    List<ExerciseHistory> findAllByIdOrderByIdDesc(long id);

    List<ExerciseHistory> findAllByCustomer_IdAndStartDate(long exerciseCustomerId, LocalDate searchDate);
}
